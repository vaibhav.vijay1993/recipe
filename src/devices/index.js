const devices = {
    md: '(min-width: 768px)',
    lg: '(min-width: 1440px)',
};

export default devices;
