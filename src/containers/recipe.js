import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faYoutube} from '@fortawesome/free-brands-svg-icons'
import {getRecipeById} from "../recipeHttpRequest";
import * as bs from 'bootstrap/dist/css/bootstrap.css';
import { Table, ListGroup } from 'react-bootstrap';
import recipeNormalizer from "../helpers";
import {HeadingGrid, Heading, RecipeInformation, ImageStyle, Ingredients, Instructions, SubHeading} from "../components/recipe";

const Recipe = () => {
    const {recipeId} = useParams();
    const [recipe, setRecipe] = useState([]);
    useEffect(() => {
        const getRecipe = async () => {
            setRecipe(await getRecipeById(recipeId))
        };
        getRecipe();
    }, [recipeId]);
    const item = recipeNormalizer(recipe)[0] || {};
    return (
        <>
            <HeadingGrid>
                <Heading>
                    {item.strMeal}
                    <a target='_blank' rel='noopener noreferrer' href={item.strYoutube}>
                        Check out the video <FontAwesomeIcon icon={faYoutube}/>
                    </a>
                </Heading>
                <ImageStyle>
                    <img src={item.strMealThumb} alt={item.strMeal}/>
                </ImageStyle>
            </HeadingGrid>
            <RecipeInformation>
                <Ingredients>
                    <SubHeading> Ingredients </SubHeading>
                    <ListGroup>
                        {item.ingredients && item.ingredients.map((item, index) => (
                            <ListGroup.Item key={index}>{item.measurement} {item.ingredient}</ListGroup.Item>
                        ))}
                    </ListGroup>
                </Ingredients>
                <Instructions>
                    <SubHeading> Method </SubHeading>
                    <Table striped bordered hover responsive>
                        <tbody>
                        {item.strInstructions && item.strInstructions.split('.').slice(0, item.strInstructions.split('.').length-1).map((item, index) => (
                            <tr key={index}>
                                <td>{index+1}</td>
                                <td>{item.trim()}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Instructions>
            </RecipeInformation>
        </>
    );
}

export default Recipe


