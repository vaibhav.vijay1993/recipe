import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {getRecipeByCategory} from "../recipeHttpRequest";
import {Card, Elevation} from "@blueprintjs/core";
import {CategoryGrid, CategoryCard, Heading} from "../components/category";

const Recipes = () => {
    const {categoryId} = useParams();
    const [allRecipes, setAllRecipes] = useState([]);
    useEffect(() => {
        const getRecipes = async () => {
            setAllRecipes(await getRecipeByCategory(categoryId))
        };
        getRecipes();
    }, [categoryId]);
    return (
        <>
            <Heading>
                {categoryId}
            </Heading>
            <CategoryGrid>
                {
                    allRecipes.map(item => (
                        <Link key={item.idMeal} to={`/recipe/${item.idMeal}/`} alt={item.strMeal}>
                            <Card interactive={true} elevation={Elevation.THREE}>
                                <CategoryCard backgroundUrl={item.strMealThumb}>
                                    <h1>{item.strMeal}</h1>
                                </CategoryCard>
                            </Card>

                        </Link>
                    ))
                }
            </CategoryGrid>
        </>
    );
}

export default Recipes
