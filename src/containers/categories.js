import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {getRecipeCategories} from "../recipeHttpRequest";
import {Card, Elevation} from "@blueprintjs/core";
import {CategoryGrid, CategoryCard, Heading} from "../components/category";

const Categories = () => {
    const [allCategories, setAllCategories] = useState([]);
    useEffect(() => {
        const getCategories = async () => {
            setAllCategories(await getRecipeCategories())
        };
        getCategories();
    }, []);
    return (
        <>
            <Heading>
                Recipe Categories
            </Heading>
            <CategoryGrid>
                {
                    allCategories.map(item => (
                        <Link key={item.idCategory} to={`/recipes/${item.strCategory}/`} alt={item.strCategory}>
                            <Card interactive={true} elevation={Elevation.THREE}>
                                <CategoryCard backgroundUrl={item.strCategoryThumb}>
                                    <h1>{item.strCategory}</h1>
                                </CategoryCard>
                            </Card>

                        </Link>
                    ))
                }
            </CategoryGrid>
        </>
    );
}

export default Categories
