import styled from 'styled-components'
import devices from "../devices";

const HeadingGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-row-gap: 10px;
    grid-column-gap: 10px;
    @media ${devices.md} {
         grid-template-columns: repeat(2,1fr);
    }
    padding: 10px 10px;
`;

const Heading = styled.div`
    align-self: center;
    display: grid;
    font-size:20px;
    justify-self: center;
    grid-row-gap: 10px;
    @media ${devices.md} {
        grid-row-gap: 0px;
         font-size:40px;
    }
    a { 
        text-decoration: none;
        color: black;
        font-size: 20px;
    }
    
    svg {
        path {
            fill: red;
        }
    }
`;

const ImageStyle = styled.div`
    justify-self: center;
    img {
        height: 200px;
    }
`;

const RecipeInformation = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-row-gap: 10px;
    grid-column-gap: 10px;
    @media ${devices.md} {
         grid-template-columns: repeat(2,1fr);
    }
    padding: 0 10px 10px 10px;
`;

const SubHeading = styled.div`
    font-size:18px;
    text-align: center;
    @media ${devices.md} {
         font-size:30px;
    }
    padding: 10px 0;
`;

const Ingredients = styled.div`
`;

const Instructions = styled.div`
`;

export {HeadingGrid, Heading, RecipeInformation, ImageStyle, Ingredients, Instructions, SubHeading}
