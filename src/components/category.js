import styled from 'styled-components'
import devices from "../devices";

const Heading = styled.div`
    font-size:50px;
    padding-bottom: 10px;
    text-align: center;
    
    @media ${devices.md} {
         font-size:80px;
    }
`;

const CategoryCard = styled.div`
    height: 200px;
    background: no-repeat center/80% url("${props => props.backgroundUrl}");
    h1 { 
        height:160px;
        background-image: linear-gradient(to bottom, rgba(255,255,255,1), rgba(255,255,255,0));
        text-align: center;
    }
`;

const CategoryGrid = styled.div`
    padding: 10px 10px;
    a {
        color: black;
        text-decoration: none;
    }
    display: grid;
    grid-template-columns: 1fr;
    grid-column-gap: 10px;
    grid-row-gap: 10px;
    
    @media ${devices.md} {
        grid-template-columns: repeat(2, 1fr);
    }
    
    @media ${devices.lg} {
        grid-template-columns: repeat(3, 1fr);
    }
`;

export { CategoryCard, CategoryGrid, Heading }
