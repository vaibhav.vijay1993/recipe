import React from 'react';
import Categories from "./containers/categories";
import Recipes from "./containers/recipes";
import Recipe from "./containers/recipe";
import {
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import './App.css';

const App = () => (
    <Router>
        <Route exact path="/" component={Categories} />
        <Route exact path="/recipes/:categoryId/" component={Recipes} />
        <Route exact path="/recipe/:recipeId/" component={Recipe} />
    </Router>
)

export default App;
