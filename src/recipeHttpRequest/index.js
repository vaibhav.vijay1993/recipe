import axios from 'axios';

const recipe = 'https://www.themealdb.com/api/json/v1/1/'
const getCategories = recipe + 'categories.php'
const getCategoryRecipe = (categoryId) => recipe + `filter.php?c=${categoryId}`
const getRecipeId = (recipeId) => recipe + `lookup.php?i=${recipeId}`

export async function getRecipeCategories() {
    try {
        return (await axios.get(getCategories)).data.categories;
    } catch (error) {
        console.error(error);
    }
}

export async function getRecipeByCategory(categoryId) {
    try {
        return (await axios.get(getCategoryRecipe(categoryId))).data.meals;
    } catch (error) {
        console.error(error);
    }
}

export async function getRecipeById(recipeId) {
    try {
        return (await axios.get(getRecipeId(recipeId))).data.meals;
    } catch (error) {
        console.error(error);
    }
}
